FROM node:latest

# Install firefox browser and the selenium driver
RUN apt-get update && \
    apt-get install -y firefox-esr

COPY ./drivers/geckodriver /usr/local/bin/
RUN chmod +x /usr/local/bin/geckodriver

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \ 
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get -y install google-chrome-stable

# install node.js app

RUN mkdir /app
WORKDIR /app

COPY LICENSE .
COPY README.md .
COPY package.json .
COPY build.txt .
COPY ./*.js ./
COPY ./*.json ./

RUN npm install

EXPOSE 3011

CMD ["node", "app.js"]