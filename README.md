# Load Generator

## Environment vars

| variable |   description    |  default value   |
|----------|------------------|------------------|
| QOTD_WEB_HOST         |  The url of the QotD front end                       |   |
| QRCODE                |  Invoke scenarios on the QRCode component            | false |
| HEADLESS              |  Used when debugging locally, run browser headless.  | true  |
| USE_FIREFOX           |  Use the Firefox browser, otherwise Chome is used.   | false |
| RANDOMIZE_SCENARIOS   |  Run scenarios in random order.                      | false |
| USE_SCHEDULE          |  Use the daily schedule to contol volume of requests | true  |

